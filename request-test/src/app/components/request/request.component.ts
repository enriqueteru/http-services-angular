import { Component, OnInit } from '@angular/core';
import {PostData, RequestService} from '../../services/request.service'

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})



export class RequestComponent implements OnInit {



  public post : PostData[] = [];

  constructor(private requestService: RequestService ) { }

  ngOnInit(): void {
   this.showData()
  }


  showData(){
    this.requestService.getData().subscribe(data => {
      this.post = data
    })
  }
}
