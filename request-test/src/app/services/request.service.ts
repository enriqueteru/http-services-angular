import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

export interface PostData {
userId: Number,
id: number,
title: string,
body: string
}

@Injectable({
  providedIn: 'root'
})
export class RequestService {


private urlAPI: string = 'https://jsonplaceholder.typicode.com/posts'

constructor(private http: HttpClient) { }

getData():Observable<PostData[]>{

    return this.http.get<PostData[]>(this.urlAPI)


  }}
